﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgo
{
    class Generation
    {
        List<Genome> genomeList = new List<Genome>();
        List<double> rad = new List<double>();
        List<double> expRad = new List<double>();
        public static Random random = new Random(DateTime.Now.Millisecond);
        int NUM_THREADS = 1;
        public Generation(List<double> rads, List<double> expected,int numthreads)
        {
            NUM_THREADS = numthreads;
            rad = rads;
            expRad = expected;
            for (int i = 0; i < 20; i++)
            {
                genomeList.Add(new Genome());
            }
        }

        public string [] listGenomes(){
            string[] strlist = new string[genomeList.Count];
            
            for (int i = 0; i < genomeList.Count; i++)
            {
                strlist[i] = genomeList[i].prtSingRow();
            }
            return strlist;
        }

        public void fitness()
        {
            double[] tmpLMSE = new double[20];
             System.Threading.Tasks.Parallel.For(0, 20, new ParallelOptions { MaxDegreeOfParallelism = NUM_THREADS },i =>
            {
                genomeList[i].computeF(rad, expRad);
               // Console.WriteLine(i);
            });

             System.Threading.Tasks.Parallel.For(0, 20, new ParallelOptions { MaxDegreeOfParallelism = NUM_THREADS }, i =>
             {
                 genomeList[i].fixOrder();
                 // Console.WriteLine(i);
             });
            for (int i = 0; i < 20; i++)
            {
                //genomeList[i].computeF(rad, expRad);
                
                tmpLMSE[i] = genomeList[i].getLMSE();
               // Console.WriteLine(tmpLMSE[i].ToString());
            }
            List<Genome> SortedList = genomeList.OrderBy(o => o.getLMSE()).ToList();

            genomeList = SortedList;
            //Console.WriteLine("FitnessOrder\n");

            genomeList.RemoveRange(9,10);// RemoveRange(10, genomeList.Count - 1);
           
            //makesure list after finess is in proper order and lowest 10 removed
            //for (int i = 0; i < genomeList.Count; i++)
           // {
             //   Console.WriteLine(genomeList[i].getLMSE().ToString());
           // }
        }
        public void crossover()
        {
            int crossIndx1 = 0;
            int crossIndx2 = 0;
            Boolean[] crossed = { false, false, false, false, false, false, false, false, false, false };
            Boolean OK = false;
            while (!allCrossed(crossed))
            {
                while(!OK)
                {
                    crossIndx1 = random.Next(0, 10);//should get an int 0 - 9 because rounds down
                    //Console.WriteLine("NOTOK    "+crossed[crossIndx1].ToString() + "   ");
                    if (crossed[crossIndx1]== false)
                    {
                        OK = true;
                        crossed[crossIndx1] = true;
                    }
                }
                OK = false;
                while (!OK)
                {
                    crossIndx2 = random.Next(0, 10);//should get an int 0 - 9 because rounds down

                    if (crossed[crossIndx2] == false)
                    {
                        OK = true;
                    }
                    if (crossIndx1 == crossIndx2)
                    {
                        OK = false;
                    }
                    else
                    {
                        crossed[crossIndx2] = true;
                    }
                }
                OK = false;
            
                        //Here can do crossover & add child to Generations
                if (genomeList.Count < 20)
                {
                    genomeList.Add(genomeList[crossIndx1].crossover(genomeList[crossIndx2]));
                }

            }
        }

        //Mutates the Genome in the generation if random number is greater than probwillMut
        public void mutate(double probWillMut, double mutRange)
        {
             Random rand = new Random(DateTime.Now.Millisecond);
             double randNum;
            for (int i = 0; i < genomeList.Count; i++)
            {
                randNum = rand.NextDouble();
               // Console.WriteLine(randNum);
                if (randNum < probWillMut)
                {
                    genomeList[i].mutate(mutRange);
                    //Console.WriteLine("MUtated Genome " + i);
                }
                
            }
        }

        //Gets the current best genome in the Generation
        public Genome getBest()
        {
            int bstInd = 0;
            for (int i = 1; i < genomeList.Count; i++)
            {
                if (genomeList[bstInd].getLMSE() > genomeList[i].getLMSE() && genomeList[i].getLMSE() != 0)
                {
                    bstInd = i;
                }
            }
            return genomeList[bstInd];
        }
        public Boolean allCrossed(Boolean[] x)
        {
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] == false)
                {
                    
                    return false;
                }
            }
            return true;
        }//End allCrossed

    }//End Class Generation
}// End namespace GeneticAlgo
