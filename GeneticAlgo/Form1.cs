﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace GeneticAlgo
{
     
    public partial class Form1 : Form
    {
        int NUM_THREADS = 0;
        List<double> rad = new List<double>();
        List<double> expRes = new List<double>();
        Generation curGen;
        int numGenerations = 0;
        Thread mainSimThd;
        public Form1()
        {
            InitializeComponent();
            //mainSimThd= new Thread(new ThreadStart(startSim));
        }
        public void startSim()
        {
            Genome bestGenome;
            Genome testGenome = null;
            int cnt = 0;
            while (cnt < Convert.ToInt32(numGenText.Text))
            {
              
                //Perform Fitnessfuction on  Current Generation
                curGen.fitness();
               // genList.Items.Clear();
                //genList.Items.AddRange(curGen.listGenomes());

                //Perfrom CrossOver on Survivors of Current Generation
                curGen.crossover();//Crosses parents to make 5 children
                curGen.crossover();//Crosses parents to make 5 childrend 
                //Total Generation size should BeginInvoke 20 now
                
                //Possibly Mutate a Genome in new Generation
                curGen.mutate(Convert.ToDouble(mutText.Text),Convert.ToDouble(textBox1.Text));
               
                cnt++;

                bestGenome = curGen.getBest();
                if(testGenome == null)
                {
                    testGenome = new Genome(bestGenome.getGenome());
                }
               
                else if (testGenome.getLMSE() > bestGenome.getLMSE()||testGenome.getLMSE() == 0.0)
                {
                    testGenome = bestGenome;
                }
                //Allows for updating form from This thread
                this.Invoke((MethodInvoker)delegate {
                  // genList.Items.Clear(); // runs on UI thread
                  // genList.Items.AddRange(curGen.listGenomes());
                    
                    lmseText.Text = testGenome.getLMSE().ToString();
                    bestList.Items.Clear();
                    bestList.Items.AddRange(testGenome.atojStr());
                });
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                NUM_THREADS = 1;
            }
            else if (radioButton2.Checked)
            {
                NUM_THREADS = 2;
            }
            if (radioButton3.Checked)
            {
                NUM_THREADS = 4;
            }
            //Show Initial Generation
            curGen = new Generation(rad, expRes, NUM_THREADS);
            genList.Items.Clear();
            genList.Items.AddRange(curGen.listGenomes());
            
            mainSimThd = new Thread(new ThreadStart(startSim));
            mainSimThd.Start();
         }

        private void button2_Click(object sender, EventArgs e)
        {

            // Stream myStream;
            OpenFileDialog openFileDialog1 = new OpenFileDialog(); openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
              
                    // Insert code to read the stream here.
                    string file = openFileDialog1.FileName;// Get file name

                    using (var rd = new StreamReader(file))
                    {
                        rd.ReadLine().Split(',');//Skip first line of text
                        while (!rd.EndOfStream)
                        {
                            var splits = rd.ReadLine().Split(',');
                            
                            rad.Add(Convert.ToDouble(splits[0]));
                            expRes.Add(Convert.ToDouble(splits[1]));
                        }
                    }
                    //genList.Visible = true;
                    bestList.Visible = true;
                    
                    startBut.Visible = true;
                    button2.Visible = false;
                    
            }
            

        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            curGen.fitness();
            genList.Items.Clear();
            genList.Items.AddRange(curGen.listGenomes());
           

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            curGen.crossover();//Crosses parents to make 5 children
            curGen.crossover();//Crosses parents to make 5 childrend 
            //Total Generation size should BeginInvoke 20 now
            genList.Items.Clear();
            genList.Items.AddRange(curGen.listGenomes());
        }

        private void mutBut_Click(object sender, EventArgs e)
        {
            curGen.mutate(Convert.ToDouble(mutText.Text), Convert.ToDouble(textBox1.Text));
            genList.Items.Clear();
            genList.Items.AddRange(curGen.listGenomes());
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

  /*      private void button3_Click(object sender, EventArgs e)
        {
            //Print Radians
            Console.WriteLine("Radians:");
            foreach (var element in rad)
                Console.WriteLine(element);

            Console.WriteLine("Expected function result:");
            foreach (var element in expRes)
                Console.WriteLine(element);
        }
   */
    }
}
