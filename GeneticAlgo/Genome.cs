﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneticAlgo
{
    class Genome 
    {
        string[] labText = { "A: ", "B: ", "C: ", "D: ", "E: ", "F: ", "G: ", "H: ", "I: ", "J: " };
        //Initialized arrays to make sure have setvalues
        double[] atoj = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        string[] atojText = { "1", "1", "1", "1", "1", "1", "1", "1", "1", "1" };
        double LMSE;
        public static Random random = new Random();
        public Genome()
        {
            LMSE = 0;
            GenConst();
        }
        public Genome(double[] initVal)
        {
            LMSE = 0;
            atoj = initVal;
        }
        //Generates The Genome(A thru J for the Genome object)
        public void GenConst()
        {
            
            double randNum = random.NextDouble() * 10;
            for (int i = 4; i >= 0; i--)
            {
                atoj[i] = randNum;
                atojText[i] = labText[i] + randNum.ToString();
                //echo(i+":  " + atoj[i]);
                randNum = random.NextDouble() * atoj[i];
            }
            randNum = random.NextDouble();
            for (int i = 9; i >= 5; i--)
            {
                atoj[i] = randNum;
                atojText[i] = labText[i] + randNum.ToString();
                //echo(i + ":  " + atoj[i]);
                randNum = random.NextDouble() * atoj[i];
            }
            //return atoj;
        }

        public Genome crossover(Genome aGenome)
        {
            //Temp To test if be cause of sloppy crossover point
            int crossIndx = random.Next(1,10) ;//should get an int 0 - 9 because rounds down

            
            double[] tmpArry = new double[10]; 
            double[] tmpCurPar = new double[10]; 
            this.getGenome().CopyTo(tmpCurPar,0);
            aGenome.getGenome().CopyTo(tmpArry, 0);
            //start at Cross over point overwrite current Genome data with new/incoming Genome at crossover

            for (; crossIndx < 10; crossIndx++)
            {
                tmpCurPar[crossIndx] = tmpArry[crossIndx];
            }

            Genome child = new Genome(tmpCurPar);
            child.fixOrder();
            return child;
        }
        //Verifies and fixed miss ordering caused by cross over 
        //A<B<C<D<E   & F<G<H<I<J
        public void fixOrder()
        {
            double tmp = 0;
            if (atoj[9] > 10)
            {
                atoj[9]= 9.99;
            }
            if (atoj[4] < 1)
            {
                atoj[4] = 0.99;
            }
            for (int j = 0; j < 9; j++)
            {
                if (atoj[j] < 0)
                {
                    atoj[j] = atoj[j] * -1;
                }
                 
            }
            
            for (int i = 0; i < 4; i++)
            {
                if (i == 0 && atoj[i] > atoj[i + 1])
                {
                    tmp = atoj[i] - atoj[i + 1];
                    atoj[i] = tmp - (random.NextDouble() * atoj[i]);
                    
                }                
                else if (atoj[i] > atoj[i + 1] )
                {
                    tmp = atoj[i] - atoj[i + 1];
                    atoj[i] = atoj[i] - tmp;
                    atoj[i] = atoj[i] -(atoj[i+1]+(random.NextDouble() * atoj[i]));
                }
            }

            for (int i = 5; i < 8; i++)
            {
                if (i == 5 && atoj[i] > atoj[i + 1])
                {
                    tmp = atoj[i] - atoj[i + 1];
                    atoj[i] = tmp - (random.NextDouble() * atoj[i]);
                } 
                else if (atoj[i] > atoj[i + 1] )
                {
                    tmp = atoj[i] - atoj[i + 1];
                    atoj[i] = atoj[i] - (atoj[i + 1] + (random.NextDouble() * atoj[i]));
                }
            }
        }

        //Mutates the Genome in random locations based on parameters
        public void mutate(double mutRange)
        {
            int mutIndx = random.Next(0,10);//Where
            double mutVal = 0;
            if (mutIndx > 4)
            {
                if (random.Next(0, 15) % 2 == 0)
                {
                    mutVal = random.NextDouble() * 8;// *mutRange;//Base value of muation
                    atoj[mutIndx] = atoj[mutIndx] + (mutVal * mutRange);
                }
                else if (random.Next(0, 15) % 2 == 0)
                {
                    mutVal = random.NextDouble() * 8;
                    atoj[mutIndx] = atoj[mutIndx] - (5 * mutRange) / ( mutVal);
                }
                else
                {
                    mutVal = random.NextDouble() * 8;
                    atoj[mutIndx] = atoj[mutIndx] - (mutVal * mutRange);
                }

            }
            else if (mutIndx <= 4)
            {

                if (random.Next(0, 15) % 2 == 0)
                {
                    mutVal = random.NextDouble();// *mutRange;//Base value of muation
                    atoj[mutIndx] = atoj[mutIndx] + (mutVal * mutRange);
                }
                else if (random.Next(0, 15) % 2 == 0)
                {
                    mutVal = random.NextDouble() ;
                    atoj[mutIndx] = atoj[mutIndx] - ( mutRange) / (mutRange * mutVal);
                }
                else{
                    mutVal = random.NextDouble();// *mutRange;//Base value of muation
                    atoj[mutIndx] = atoj[mutIndx] - (mutVal * mutRange);
                }
              
            }
           // echo("LMSE   " + LMSE.ToString());
            //atoj[mutIndx] = atoj[mutIndx] + (mutVal * 1.5);//Actual Muation //Was 0.03
            //Example from Class atoj[mutIndx] = atoj[mutIndx] + (mutVal - 0.5) * 1;//Actual Muation
        }

        public string[] atojStr()
        {
            for (int i = 0; i < 10; i++)
            {
                atojText[i] = atoj[i].ToString();
            }
           
            return atojText;
        }
        public double getLMSE()
        {
            return LMSE;
        }
        public void computeF(List<double> rads,List<double> expected)
        {
            double fofR=0;
            double curMSE = 0;

            for(int i = 0; i< rads.Count; i++)
            {
                fofR =  atoj[5] * Math.Sin(atoj[0] * rads[i]) + atoj[6] * Math.Sin(atoj[1] * rads[i]) + 
                        atoj[7] * Math.Sin(atoj[2] * rads[i]) + atoj[8] * Math.Sin(atoj[3] * rads[i]) + atoj[9] * Math.Sin(atoj[4] * rads[i]);
                curMSE = Math.Pow((expected[i] - fofR), 2);
               // echo(curMSE.ToString());
                LMSE = LMSE + curMSE;
            }
            LMSE = LMSE / rads.Count;
        }
        //prints out genome in single row format
        public string prtSingRow()
        {
            int spaces = 0;
            string genRow = "";
            for (int ind = 0; ind < 10; ind++)
            {
                //if (ind > 0)
               // {
                    genRow = genRow + atoj[ind].ToString();
                    if (atoj[ind].ToString().Length < 18)
                    {
                        spaces = 18 - atoj[ind].ToString().Length;
                       // echo("Spaces = "+spaces.ToString() +" atojLenght= "+ (atoj[ind].ToString().Length).ToString());
                        for (int i = 0; i < spaces+4; i++)
                        {
                            genRow = genRow + " ";
                        }
                    }
                    else
                    {
                        genRow = genRow + "      ";
                    }
                    
                   // echo(genRow.Length.ToString());
                //}
                
            }
            
            return genRow;
        }

        public double[] getGenome()
        {
            return atoj;
        }

        public void echo(string str)
        {
            Console.WriteLine(str);
        }
    }
}
