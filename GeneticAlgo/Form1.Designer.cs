﻿namespace GeneticAlgo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.genList = new System.Windows.Forms.ListBox();
            this.startBut = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.bestList = new System.Windows.Forms.ListBox();
            this.fitBut = new System.Windows.Forms.Button();
            this.crossBut = new System.Windows.Forms.Button();
            this.mutBut = new System.Windows.Forms.Button();
            this.mutText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lmseText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.numGenText = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // genList
            // 
            this.genList.FormattingEnabled = true;
            this.genList.Location = new System.Drawing.Point(12, 12);
            this.genList.Name = "genList";
            this.genList.Size = new System.Drawing.Size(1158, 147);
            this.genList.TabIndex = 0;
            this.genList.Visible = false;
            // 
            // startBut
            // 
            this.startBut.Location = new System.Drawing.Point(28, 371);
            this.startBut.Name = "startBut";
            this.startBut.Size = new System.Drawing.Size(98, 37);
            this.startBut.TabIndex = 1;
            this.startBut.Text = "Start Sim";
            this.startBut.UseVisualStyleBackColor = true;
            this.startBut.Visible = false;
            this.startBut.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(28, 372);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 38);
            this.button2.TabIndex = 2;
            this.button2.Text = "Select CSV";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // bestList
            // 
            this.bestList.FormattingEnabled = true;
            this.bestList.Location = new System.Drawing.Point(33, 197);
            this.bestList.Name = "bestList";
            this.bestList.Size = new System.Drawing.Size(222, 147);
            this.bestList.TabIndex = 3;
            this.bestList.Visible = false;
            // 
            // fitBut
            // 
            this.fitBut.Location = new System.Drawing.Point(181, 372);
            this.fitBut.Name = "fitBut";
            this.fitBut.Size = new System.Drawing.Size(116, 36);
            this.fitBut.TabIndex = 5;
            this.fitBut.Text = "Fitness function";
            this.fitBut.UseVisualStyleBackColor = true;
            this.fitBut.Visible = false;
            this.fitBut.Click += new System.EventHandler(this.button3_Click);
            // 
            // crossBut
            // 
            this.crossBut.Location = new System.Drawing.Point(202, 414);
            this.crossBut.Name = "crossBut";
            this.crossBut.Size = new System.Drawing.Size(82, 29);
            this.crossBut.TabIndex = 6;
            this.crossBut.Text = "Crossover";
            this.crossBut.UseVisualStyleBackColor = true;
            this.crossBut.Visible = false;
            this.crossBut.Click += new System.EventHandler(this.button4_Click);
            // 
            // mutBut
            // 
            this.mutBut.Location = new System.Drawing.Point(181, 442);
            this.mutBut.Name = "mutBut";
            this.mutBut.Size = new System.Drawing.Size(111, 38);
            this.mutBut.TabIndex = 8;
            this.mutBut.Text = "Mutate";
            this.mutBut.UseVisualStyleBackColor = true;
            this.mutBut.Visible = false;
            this.mutBut.Click += new System.EventHandler(this.mutBut_Click);
            // 
            // mutText
            // 
            this.mutText.Location = new System.Drawing.Point(355, 404);
            this.mutText.Name = "mutText";
            this.mutText.Size = new System.Drawing.Size(44, 20);
            this.mutText.TabIndex = 9;
            this.mutText.Text = "0.218";
            this.mutText.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(331, 385);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Mutate Probability";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Best Genome";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Best LMSE";
            // 
            // lmseText
            // 
            this.lmseText.Location = new System.Drawing.Point(28, 139);
            this.lmseText.Name = "lmseText";
            this.lmseText.Size = new System.Drawing.Size(149, 20);
            this.lmseText.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "A:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 210);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "B:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 223);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "C:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "D:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 253);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "E:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 266);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "F:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 278);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "G:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 291);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "H:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 304);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "I:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 317);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "J:";
            // 
            // numGenText
            // 
            this.numGenText.Location = new System.Drawing.Point(33, 437);
            this.numGenText.Name = "numGenText";
            this.numGenText.Size = new System.Drawing.Size(92, 20);
            this.numGenText.TabIndex = 24;
            this.numGenText.Text = "1000";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 422);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(118, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Number Of Generations";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(336, 452);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(96, 20);
            this.textBox1.TabIndex = 26;
            this.textBox1.Text = "10";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(338, 437);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 13);
            this.label15.TabIndex = 27;
            this.label15.Text = "Mutation range";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(75, 525);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(68, 17);
            this.radioButton1.TabIndex = 28;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "1 Thread";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(202, 525);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(73, 17);
            this.radioButton2.TabIndex = 29;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "2 Threads";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(326, 525);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(73, 17);
            this.radioButton3.TabIndex = 30;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "4 Threads";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 559);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.numGenText);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lmseText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mutText);
            this.Controls.Add(this.mutBut);
            this.Controls.Add(this.crossBut);
            this.Controls.Add(this.fitBut);
            this.Controls.Add(this.bestList);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.startBut);
            this.Controls.Add(this.genList);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox genList;
        private System.Windows.Forms.Button startBut;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox bestList;
        private System.Windows.Forms.Button fitBut;
        private System.Windows.Forms.Button crossBut;
        private System.Windows.Forms.Button mutBut;
        private System.Windows.Forms.TextBox mutText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox lmseText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox numGenText;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
    }
}

